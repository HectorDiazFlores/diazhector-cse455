//
//  ViewController.swift
//  USDtoYEN
//
//  Created by hector on 4/7/18.
//  Copyright © 2018 csusb455. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //initialization of the result text field
    @IBOutlet weak var ResultTextfield: UILabel!
    //initialization of the label text field
    @IBOutlet weak var USDtextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //initialization of the submit button
    @IBAction func SubmitButton(_ sender: Any) {
        
        //get value from the button, turn it into a double and store it as 'textFieldValue'
        let textFieldValue = Double(USDtextfield.text!)
        if textFieldValue != nil {
            let result = Double(textFieldValue! * 112.57)
            
            //have the converted value display on the result text field
            ResultTextfield.text = "$\(textFieldValue!) = ￥\(result)"
            USDtextfield.text = ""
        } else{
            ResultTextfield.text = "The Text field cannot be left blank"
            
        }
        
        
    }


    

}

